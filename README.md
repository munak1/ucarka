### Ucarka

Explore more platforms: [Crypto Chooser](https://cryptochooser.com/)

This project is based on a GitLab [Project Template](https://docs.gitlab.com/ee/gitlab-basics/create-project.html).

Improvements can be proposed in the [original project](https://gitlab.com/gitlab-org/project-templates/express).

### CI/CD with Auto DevOps

This template is compatible with [Auto DevOps](https://docs.gitlab.com/ee/topics/autodevops/).

If Auto DevOps is not already enabled for this project, you can [turn it on](https://docs.gitlab.com/ee/topics/autodevops/#enabling-auto-devops) in the project settings.



## Introduction

This project is a bitcoin exchange based on Java (SpringCloud) | BTC exchange | ETH exchange | digital currency exchange | trading platform | matching trading engine. This project is based on the development of Spring Cloud microservices, which can be used to build and secondary development digital currency exchanges, and has a complete system component.
- Match trading engine. 
- Background management (back-end + front-end)  
- Frontend (transaction page, event page, personal center, etc.)） 
- Native Android APP source code 
- Native Apple APP source code 
- Currency wallet RPC source code 
- Multi Language Support（Simple Chinese、English）

## About Cloud

Spring Cloud is an ordered collection of frameworks. It uses Spring Boot's development convenience to subtly simplify the development of distributed system infrastructure, such as service discovery registration, configuration center, message bus, load balancing, circuit breaker, data monitoring, etc., can be done using Spring Boot's development style One click to start and deploy. Spring Cloud does not repeat the manufacturing of wheels. It just combines the mature and practical service frameworks developed by various companies. The re-encapsulation through the Spring Boot style shields the complex configuration and implementation principles, and finally gives the development The author has left a set of distributed system development kits that are simple to understand, easy to deploy, and easy to maintain.
In general, a complete Spring Cloud framework should be as shown in the following figure:

u are unfamiliar with Spring Cloud, you can simply learn about Spring Cloud related tutorials first, so that you will come back to this project and it will be easier to get started.
As a reminder, because the Springcloud framework diagram is a complete architecture, we will tailor some content appropriately during development to make development and deployment faster, so there are some discrepancies.

## Core function description (user side)

- [x] 1.  Registration/login/real-name authentication/audit (currently only supports mobile phones, secondary development can be added to the mail, very simple)
- [x] 2. Banner/Announcement/Help/Customized page (Banner supports separate settings for PC and APP, helps support various classification modes)  
- [x] 3. Fiat currency C2C transaction/fiat currency OTC transaction (supports two fiat currency models, the platform can undertake C2C fiat currency exchange in the early stage of the project, and OTC transactions can be opened later)  
- [x] 4. Coin trading (support for limit order, market order, and other commission modes can be added for secondary development)  
- [x] 5. Invite registered/promotion partners (support for ranking statistics on the number of invited promoters and commissions by day, week and month)  
- [x] 6. Innovation Lab (there are many supported functions in this part, and itemized explanations. In addition, the APP does not support this function for the time being)  

> - [x] 6-1. First launch panic buying activity mode (for example, when issuing a new trading pair, a certain number of currencies can be set on the trading pair for panic buying)  
> - [x] 6-2. The first distribution mode of activity (for example, before issuing the BTC/USDT trading pair, the official took out 5BTC for the activity, and divided the BTC equally according to how much USDT the user recharged mortgaged)  
> - [x] 6-3. Panic buying mode (for example, before the ZZZ/USDT trading pair is issued, the ZZZ currency price is 5USDT, and the official issuance activity price is 0.5USDT, you can use this mode)  
> - [x] 6-4. Control panel sharing mode (such as 6-3, only average distribution)  
> - [x] 6-5. Mining machine activity mode (support users to mortgage a certain amount of currency, and the official promise to return a certain amount of currency every month)  

- [x] 7. Red envelope function (support platform and official to issue a certain amount of currency red envelope, this function is suitable for user fission)  
- [x] 8. Various basic management such as user asset management, flow management, commission management, real name management  

## Core function description (management side) 

- [x] 1. Summary (view platform operating data, including transaction amount, registered number, recharge, etc.)  
- [x] 2. Member management (member information management, member real name verification, member real name management, member balance management, member recharge/freeze balance, etc.)  
- [x] 3. Invitation management (member invitation information, member invitation ranking management)  
- [x] 4. CTC management (CTC order management, flow management, acceptor management)  
- [x] 5. Content management (PC advertising management, APP advertising management, announcement management, help management)  
- [x] 6. Financial management (charge and withdrawal management, financial flow management, reconciliation management, currency wallet balance management)  
- [x] 7. Currency management (new trading pair, management trading pair, new trading robot, setting trading robot parameters, setting market engine/trading engine, revoking all orders)  
- [x] 8. Event management (new activity, mining machine subscription, panic buying/division management)  
- [x] 9. Red envelope management (platform red envelope management, user red envelope management)  
- [x] 10. System management (role management, department management, user management, authority management, currency management, RPC management, version management)  
- [x] 11. Margin management (this function is considered in design, but not used during actual operation)  
- [x] 12. OTC management (advertising management, order management, OTC currency management, surrender management, etc., this function has not been verified by actual operation)  


## Attention

Anyone who uses this source code to engage in commercial activities and causes losses to others and himself is not responsible for me!

